import types from './types'
import { local, update } from './reducers/getUpdate'
// @ts-ignore
import { log } from '../config/settings'

// Actions
const actions = {
  async getLocal({ state, commit }: any) {
    log && console.log('run getLocal in actions')
    commit(types.GET_LOCAL, await local(state))
  },
  async getUpdate({ state, commit }: any) {
    log && console.log('run getUpdate in actions')
    commit(types.GET_UPDATE, await update(state, false))
  },
  tick({ commit }: any) {
    // console.log('run tick in actions')
    commit(types.TICK)
  },
}

export default actions
