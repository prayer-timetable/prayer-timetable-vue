// Getters
const getters = {
  isEven({ count }: any) {
    return count % 2 === 0
  },
}

export default getters
