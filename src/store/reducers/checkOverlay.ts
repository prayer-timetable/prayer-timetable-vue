// @ts-ignore
import {
  toDate,
  getYear,
  getMonth,
  getDate,
  getDay,
  addHours,
  setDay,
  isWithinInterval,
} from 'date-fns'

import { hijriConvert } from '../../helpers/func'

const checkOverlay = (state: any) => {
  const { settings } = state
  const now = new Date()

  // console.log(getDay(now))
  // JUMMUAH
  const jummuah =
    getDay(now) === 5
      ? setDay(
          toDate(
            new Date(
              getYear(now),
              getMonth(now),
              getDate(now),
              settings.jummuahtime[0],
              settings.jummuahtime[1]
            )
          ),
          5
        )
      : null
  const isJummuah = jummuah
    ? isWithinInterval(now, {
        start: jummuah,
        end: addHours(jummuah, 1),
      })
    : false
  // console.log(isJummuah)

  // TARAWEEH
  const taraweeh =
    hijriConvert(now).hm === 9
      ? setDay(
          toDate(
            new Date(
              getYear(now),
              getMonth(now),
              getDate(now),
              settings.jummuahtime[0],
              settings.jummuahtime[1]
            )
          ),
          5
        )
      : null
  const isTaraweeh = taraweeh
    ? isWithinInterval(now, {
        start: taraweeh,
        end: addHours(taraweeh, 2),
      })
    : false

  // console.log(isTaraweeh)

  if (isJummuah) {
    return 'Jummuah'
  }
  if (isTaraweeh) {
    return 'Taraweeh'
  }

  return ''
}

export default checkOverlay
