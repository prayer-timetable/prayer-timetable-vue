// types
const types = {
  GET_UPDATE: 'GET_UPDATE',
  GET_LOCAL: 'GET_LOCAL',
  TICK: 'TICK',
}

export default types
