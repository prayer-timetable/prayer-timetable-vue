import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { format } from 'date-fns'

import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
})

export default store

export interface State {
  timetable: object
  settings: {
    title: string
    description: string
    online: boolean
    log: boolean
    updateInterval: number
    url: {
      settings: string
      timetable: string,
    }
    announcement: string
    text: {
      en: string
      ar: string,
    }
    hijrioffset: string
    join: string
    jummuahtime: number[]
    taraweehtime: number[]
    jamaahShow: boolean
    jamaahmethods: string[]
    jamaahoffsets: number[][]
    updated: number,
  }
  prayers: {
    countDown: {
      duration: any
      name: string
      time: any,
    }
    countUp: {
      duration: any
      name: string
      time: any,
    }
    current: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    focus: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    hijri: any
    isAfterIsha: boolean
    isJamaahPending: boolean
    next: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    now: any
    percentage: number
    prayers: {
      today: object
      tomorrow: object
      yesterday: object,
    }
    previous: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    },
  }
  day: {
    now: any
    hijri: any
    overlayActive: boolean
    overlayTitle: string,
  }
  update: {
    downloaded: number
    refreshed: any
    success: boolean,
  }
}

const a = {
  prayers: {
    today: [
      {
        time: '2019-06-07T01:53:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T02:08:00.000Z',
        index: 0,
        hasPassed: true,
        name: 'fajr',
        when: 'today',
        dstAdjust: 1,
      },
      {
        time: '2019-06-07T03:56:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T03:56:00.000Z',
        index: 1,
        hasPassed: true,
        name: 'shurooq',
        when: 'today',
        dstAdjust: 1,
      },
      {
        time: '2019-06-07T12:26:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T12:45:00.000Z',
        index: 2,
        hasPassed: true,
        name: 'dhuhr',
        when: 'today',
        dstAdjust: 1,
      },
      {
        time: '2019-06-07T16:51:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T17:06:00.000Z',
        index: 3,
        hasPassed: false,
        name: 'asr',
        when: 'today',
        dstAdjust: 1,
      },
      {
        time: '2019-06-07T20:51:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T21:06:00.000Z',
        index: 4,
        hasPassed: false,
        name: 'maghrib',
        when: 'today',
        dstAdjust: 1,
      },
      {
        time: '2019-06-07T22:30:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-07T22:30:00.000Z',
        index: 5,
        hasPassed: false,
        name: 'isha',
        when: 'today',
        dstAdjust: 1,
      },
    ],
    yesterday: [
      {
        time: '2019-06-06T01:54:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T02:09:00.000Z',
        index: 0,
        hasPassed: true,
        name: 'fajr',
        when: 'yesterday',
        dstAdjust: 1,
      },
      {
        time: '2019-06-06T03:57:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T03:57:00.000Z',
        index: 1,
        hasPassed: true,
        name: 'shurooq',
        when: 'yesterday',
        dstAdjust: 1,
      },
      {
        time: '2019-06-06T12:26:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T12:45:00.000Z',
        index: 2,
        hasPassed: true,
        name: 'dhuhr',
        when: 'yesterday',
        dstAdjust: 1,
      },
      {
        time: '2019-06-06T16:50:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T17:05:00.000Z',
        index: 3,
        hasPassed: false,
        name: 'asr',
        when: 'yesterday',
        dstAdjust: 1,
      },
      {
        time: '2019-06-06T20:50:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T21:05:00.000Z',
        index: 4,
        hasPassed: false,
        name: 'maghrib',
        when: 'yesterday',
        dstAdjust: 1,
      },
      {
        time: '2019-06-06T22:30:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-06T22:30:00.000Z',
        index: 5,
        hasPassed: false,
        name: 'isha',
        when: 'yesterday',
        dstAdjust: 1,
      },
    ],
    tomorrow: [
      {
        time: '2019-06-08T01:52:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T02:07:00.000Z',
        index: 0,
        hasPassed: true,
        name: 'fajr',
        when: 'tomorrow',
        dstAdjust: 1,
      },
      {
        time: '2019-06-08T03:56:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T03:56:00.000Z',
        index: 1,
        hasPassed: true,
        name: 'shurooq',
        when: 'tomorrow',
        dstAdjust: 1,
      },
      {
        time: '2019-06-08T12:26:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T12:45:00.000Z',
        index: 2,
        hasPassed: true,
        name: 'dhuhr',
        when: 'tomorrow',
        dstAdjust: 1,
      },
      {
        time: '2019-06-08T16:51:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T17:06:00.000Z',
        index: 3,
        hasPassed: false,
        name: 'asr',
        when: 'tomorrow',
        dstAdjust: 1,
      },
      {
        time: '2019-06-08T20:52:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T21:07:00.000Z',
        index: 4,
        hasPassed: false,
        name: 'maghrib',
        when: 'tomorrow',
        dstAdjust: 1,
      },
      {
        time: '2019-06-08T22:30:00.000Z',
        isJamaahPending: false,
        jtime: '2019-06-08T22:30:00.000Z',
        index: 5,
        hasPassed: false,
        name: 'isha',
        when: 'tomorrow',
        dstAdjust: 1,
      },
    ],
  },
  previous: {
    time: '2019-06-07T03:56:00.000Z',
    isJamaahPending: false,
    jtime: '2019-06-07T03:56:00.000Z',
    index: 1,
    hasPassed: true,
    name: 'shurooq',
    when: 'today',
    dstAdjust: 1,
  },
  current: {
    time: '2019-06-07T12:26:00.000Z',
    isJamaahPending: false,
    jtime: '2019-06-07T12:45:00.000Z',
    index: 2,
    hasPassed: true,
    name: 'dhuhr',
    when: 'today',
    dstAdjust: 1,
  },
  next: {
    time: '2019-06-07T16:51:00.000Z',
    isJamaahPending: false,
    jtime: '2019-06-07T17:06:00.000Z',
    index: 3,
    hasPassed: false,
    name: 'asr',
    when: 'today',
    dstAdjust: 1,
  },
  countUp: {
    name: 'dhuhr jamaah',
    time: '2019-06-07T12:45:00.000Z',
    duration: 14309,
  },
  countDown: { name: 'asr', time: '2019-06-07T16:51:00.000Z', duration: 451 },
  now: '2019-06-07T16:43:29.266Z',
  hijri: '2019-06-07T16:43:29.266Z',
  percentage: 96.94,
  isAfterIsha: false,
  isJamaahPending: false,
  focus: {
    time: '2019-06-07T16:51:00.000Z',
    isJamaahPending: false,
    jtime: '2019-06-07T17:06:00.000Z',
    index: 3,
    hasPassed: false,
    name: 'asr',
    when: 'today',
    dstAdjust: 1,
  },
}
