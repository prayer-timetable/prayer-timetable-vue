module.exports = {
  publicPath: './',
  // sass file to be included in all
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/config/theme.sass";`,
      },
    },
  },
  // svg loader
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use('vue-svg-loader').loader('vue-svg-loader')
  },
  // use pug
  pages: {
    index: {
      entry: './src/main.ts',
      template: './src/index.pug',
    },
  },
  // lib export
  configureWebpack: {
    output: {
      libraryExport: 'default',
    },
  },
}
